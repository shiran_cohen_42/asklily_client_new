// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

//  in the server ====>>>> host: 'ask.asklily.co',
// Use this server unless you are locally debugging the server and you should
// use ====>>>> host: 'localhost',
/*
Since the server redirects automatically to port 3000 and in local server we need 3000
*/ 
export const environment = {
  production: false,
  name: 'dev',
  host: 'https://dev-server.asklily.co',
  // host: 'http://localhost:3000',
  retailer: 'rinazin',
  firebase: {
    apiKey: 'AIzaSyD4koeTbmDoK22v6k9j93cLCbiXz0mgF_M',
    authDomain: 'asklilybackoffice.firebaseapp.com',
    databaseURL: 'https://asklilybackoffice.firebaseio.com',
    projectId: 'asklilybackoffice',
    storageBucket: 'asklilybackoffice.appspot.com',
    messagingSenderId: '1048996602555',
    appId: '1:1048996602555:web:1384d420ee74b403a23f9e',
    measurementId: 'G-SK5MHJ71R0'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

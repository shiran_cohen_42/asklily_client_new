import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FirebaseService } from 'src/app/firebased/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  userForm!: FormGroup;

  constructor(private firebaseService: FirebaseService, private formBuilder: FormBuilder,private router: Router) { }

  createUserForm(): any {
    this.userForm = this.formBuilder.group(
      {
        email: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
      }
    );
  }

  createUser(): void {
    if (this.userForm.valid) {
      this.firebaseService.createUser(this.userForm.get('email')?.value, this.userForm.get('password')?.value);
    }
  }

  backToHome(): void {
    this.router.navigate(["home-base"]);
  }
  

  ngOnInit() {
    this.createUserForm();
  }

}

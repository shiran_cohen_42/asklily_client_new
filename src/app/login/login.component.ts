import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../firebased/firebase.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  // isLogin = true;

  constructor(private firebaseService: FirebaseService, private formBuilder: FormBuilder,) { }

  createLoginForm(): any {
    this.loginForm = this.formBuilder.group(
      {
        email: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
      }
    );
  }

  login(): void {
    if (this.loginForm.valid) {
      this.firebaseService.login(this.loginForm.get('email')?.value, this.loginForm.get('password')?.value);
    }
  }

  // signup(): void {
  //   if (this.loginForm.valid) {
  //     this.firebaseService.signUp(this.loginForm.get('email')?.value, this.loginForm.get('password')?.value);
  //   }
  // }

  // showLogin(): void {
  //   this.loginForm.reset()
  //   this.isLogin = true;
  // }

  // showSignup(): void {
  //   this.loginForm.reset()
  //   this.isLogin = false
  // }

  ngOnInit() {
    this.createLoginForm();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitemTableComponent } from './item-table.component';

describe('ItemTableComponent', () => {
  let component: CitemTableComponent;
  let fixture: ComponentFixture<CitemTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitemTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitemTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ItemsService, CTableData } from '../app.itemsService';
import { AppConfigService } from '../app.configService';
import { CUser, I_UTR } from '../../../../gam_ve_gam/CUser';
import { C_Cookies } from '../C_Cookies';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-item-table',
  templateUrl: './item-table.component.html',
  styleUrls: ['./item-table.component.scss']
})
export class CitemTableComponent implements OnInit {
  numItemsLeft = -1;
  displayedColumns: string[] = ['id', 'tagged', 'category'];

  data: CTableData[] = [];
  isLoadingResults = true;
  mShowAll = false;
  mSelectedCategory = '';
  mCategoryList: string[] = [];
  aUser!: CUser;
  mCS: C_Cookies;
  haveUser = false;

  constructor(
    private api: ApiService,
    private itemService: ItemsService,
    private cookieService: CookieService,
    private config: AppConfigService) {

      // since item service is younger in production - I place the two there... change in next ver
      const tmp = this.itemService.getItemSelectOptions();
      this.mSelectedCategory = tmp[0];
      this.mShowAll = tmp[1];
      this.mCS = new C_Cookies(cookieService);
  }

  async newUser() {
    const defUtr = this.config.getTempUTR();
    await this.config.getAUser(defUtr)
      .then((data) => {
        if (data !== undefined) {
          this.aUser = data;
          this.haveUser = true;
          console.log('A new user from config');
        }
      });
  }

  async setUser(): Promise<void> {
    await this.newUser()
      .then(() => {
        const cn = this.aUser.getUT();
        const savedUserInCookies = this.mCS.getUserFromCooclies(cn);
        if (savedUserInCookies !== undefined) {
          this.aUser.update(savedUserInCookies);
          console.log('User has been modified by cookies');
        }
      });
  }

  async ngOnInit() {

    this.isLoadingResults = true;

    await this.setUser()
      .then(() => {
        console.assert(this.aUser !== undefined);
        const utr = this.aUser.getUTR();
        this.mCategoryList = this.config.getCategoryList(utr.userType);

        console.log('item table init: ', this.aUser.getTheFullName());
        this.itemService.setCurrentItems(utr)
          .then(() => {
            this.data = this.itemService.getRowData(this.mSelectedCategory, this.mShowAll);
            this.numItemsLeft = this.data.length;
            this.isLoadingResults = false;
          })
          .catch(error => console.log(error));
      });
  }

  getRowData(): void {
    this.data = this.itemService.getRowData(this.mSelectedCategory, this.mShowAll);
    this.numItemsLeft = this.data.length;
    this.isLoadingResults = false;
  }

  async doneLoading(): Promise<void> {
    console.log('Waiting for load to complete');
    while (this.itemService.loadInProgress) {
      await new Promise(res => setTimeout(res, 100));
      console.log('.');
    }
    console.log('Load compleated');
  }

  async onRetailerChange(event: any) {
    if (event !== undefined) {
      // const utr = event as I_UTR;
      // this.config.set(utr)
      //   .then(() => {
          this.ngOnInit();
        // });
    }
  }

  onItemsSelectChange(event: any) {
    this.ngOnInit();
  }

  onSelectShowCategory(event: any) {
    this.mSelectedCategory = event;
    console.log(this.mShowAll);
    this.ngOnInit();
  }

  onShowAll() {
    this.mShowAll = !this.mShowAll;
    console.log(this.mShowAll);
    this.ngOnInit();
  }
}

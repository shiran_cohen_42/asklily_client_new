import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { CdbItem } from '../../../gam_ve_gam/C_DbItem';
import { environment } from '../environments/environment';
import { I_UTR } from '../../../gam_ve_gam/CUser';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

////   MICAH https == http
const apiUrl = environment.host + '/api';
const itemsUrl = environment.host + '/items';

@Injectable({
  providedIn: 'root'
})
// @Injectable()
export class ApiService {
  bItemsIn = false;

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  async getTheItems(ut: I_UTR): Promise<CdbItem[]> {
    console.log('API items: ', ut.userType, '+', ut.retailer);
    let bItemsIn = false;
    const rv: CdbItem[] = [];
    const url = `${itemsUrl}/${ut.retailer}_${ut.userType}`;
    this.http.get<CdbItem[]>(url)
      .subscribe(items => {
        items.forEach(it => {
          const jItem = JSON.stringify(it);
          const dbItem = new CdbItem(jItem);
          rv.push(dbItem);
        });
        console.log(ut.retailer, ' ut: ', ut.userType, ' items found: ', rv.length);
        bItemsIn = true;
      },
        error => {
          console.log(error);
        }
      );

    while (!bItemsIn) {
      await new Promise(res => setTimeout(res, 5));
    }
    // await this.waitForItems();
    return rv;
  }

  private async waitForItems(): Promise<void> {
    do {
      await new Promise(res => setTimeout(res, 5));
    } while (!this.bItemsIn);
  }
  getTheItems__(ut: I_UTR): Observable<CdbItem[]> {
    console.log('API items: ', ut.userType, '+', ut.retailer);
    const url = `${itemsUrl}/${ut.retailer}_${ut.userType}`;
    return this.http.get<CdbItem[]>(url)
      .pipe(
        tap(items => console.log('fetched items for ' + ut.retailer + ' userType ' + ut.userType)),
        catchError(this.handleError('getTheItems', []))
      );
  }

  getItemById(id: string): Observable<CdbItem> {
    console.assert(id !== null, 'getItemById by id with null id');
    const url = `${apiUrl}/${id}`;
    return this.http.get<CdbItem>(url).pipe(
      tap(_ => console.log(`fetched items id=${id}`)),
      catchError(this.handleError<CdbItem>(`getItemById id=${id}`))
    );
  }


  updateItem(item: CdbItem): Observable<any> {
    console.assert(item !== null && item._id.length > 0, 'Check befor calling update');
    const url = `${apiUrl}/${item._id}`;
    return this.http.put(url, item, httpOptions).pipe(
      tap(_ => console.log(`return updated id=${item._id}`)),
      catchError(this.handleError<any>('updateItem'))
    );
  }

  addItem(item: CdbItem): Observable<CdbItem> {
    const url = `${apiUrl}/`;
    return this.http.post<CdbItem>(url, item, httpOptions).pipe(
      tap((it: CdbItem) => console.log(`adding item id=${it.mId}`)),
      catchError(this.handleError<CdbItem>('addItem'))
    );
  }

  addCCollection(items: CdbItem[]): Observable<CdbItem[]> {
    const url = `${apiUrl}/_`;
    return this.http.post<CdbItem[]>(url, items, httpOptions).pipe(
      tap(_ => console.log(`adding collections of items`)),
      catchError(this.handleError<CdbItem[]>('addItem'))
    );
  }

  deletecItem(id: string): Observable<CdbItem> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<CdbItem>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted item id=${id}`)),
      catchError(this.handleError<CdbItem>('deleteItem'))
    );
  }

}

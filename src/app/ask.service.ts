import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { CAskLilyResults } from '../../../gam_ve_gam/C_ResultDef';
import { C_Catalog } from '../../../gam_ve_gam/C_Catalog';
import { I_GuiConfig } from '../../../gam_ve_gam/I_GuiConfig';
import { CUser } from '../../../gam_ve_gam/CUser';
import { Observable, of, throwError, Subject } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

/*
  NOTICE - this file is duplicated in ASK_WC!! and ASK_BACKOFFICE (know as client)
  Make sure to keep them the same....
*/

////   MICAH https == http

const askUrl = environment.host + '/ask';

@Injectable({
  providedIn: 'root'
})
export class AskService {
  matchResults!: CAskLilyResults;
  bGotResults = false;
  bCatalog = false;
  bRetailer = false;
  bList = false;

  numItemsSelected = 0;

  constructor(private http: HttpClient) {
    console.log(`askUrl is: ${askUrl}`);
  }

  // // tslint:disable-next-line: typedef
  // private handleError<T>(operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {
  //     console.error(error);
  //     return of(result as T);
  //   };
  // }

  async askRetailerList(): Promise<string[]> {
    const rv: string[] = [];
    this.bList = false;
    const url = askUrl + '/retailerslist';
    this.http.get<string[]>(url)
      .subscribe(
        list => {
          console.log('fetched retailer list');
          list.forEach(rn => {
            rv.push(rn);
          });
          this.bList = true;
        },
        error => {
          console.log('Failed to get list: ', error);
        }
      );
    await this.waitForResultList();
    return rv;
  }

  async askCatalogs(): Promise<C_Catalog[]> {
    const rv: C_Catalog[] = [];
    this.bCatalog = false;
    const url = askUrl + '/catalogs';
    this.http.get<C_Catalog[]>(url)
      .subscribe(
        catalogs => {
          catalogs.forEach(catalog => {
            rv.push(catalog);
          });
          console.log('fetched catalogs');
          this.bCatalog = true;
        },
        error => {
          console.log('Failed to get list: ', error);
        }
      );
    await this.waitForResultCatalog();
    return rv;
  }

  async askRetailerGuiConfig(retailerName: string): Promise<I_GuiConfig> {
    let rv: any;
    console.assert(retailerName.length > 0);
    this.bRetailer = false;
    const url = `${askUrl}/${retailerName}`;
    this.http.get<I_GuiConfig>(url)
      .subscribe(
        (data: I_GuiConfig) => {
          rv = data;
          console.log('Fetched catalogs');
          this.bRetailer = true;
        },
        error => {
          console.log('Failed to get list: ', error);
        }
      );
    await this.waitForResultRetailer();
    return rv;
  }


  async askLily(user: CUser, debug: boolean): Promise<CAskLilyResults> {
    const matchResults = new CAskLilyResults(debug);

    const params: any[] = [];
    params.push(user.getRetailerName());
    params.push(user.getUser());
    params.push(debug);

    this.bGotResults = false;
    this.http.post<any>(`${askUrl}`, params)
      .subscribe(
        data => {
          this.numItemsSelected = matchResults.parseResults(data, debug);
          this.bGotResults = true;
        },
        error => {
          console.log(error);
        }
      );
    await this.waitForResultAsk();
    return matchResults;
  }


  private async waitForResultAsk(): Promise<void> {
    do {
      await new Promise(res => setTimeout(res, 5));
    } while (!this.bGotResults);
  }

  private async waitForResultCatalog(): Promise<void> {
    do {
      await new Promise(res => setTimeout(res, 5));
    } while (!this.bCatalog);
  }

  private async waitForResultList(): Promise<void> {
    do {
      await new Promise(res => setTimeout(res, 5));
    } while (!this.bList);
  }

  private async waitForResultRetailer(): Promise<void> {
    do {
      await new Promise(res => setTimeout(res, 5));
    } while (!this.bRetailer);
  }

}

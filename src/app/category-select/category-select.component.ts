import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppConfigService } from '../app.configService';

@Component({
  selector: 'app-category-select',
  templateUrl: './category-select.component.html',
  styleUrls: ['./category-select.component.scss']
})
export class CategorySelectComponent implements OnInit {
  @Input() inCatName = '';
  @Input() options: string[] = [];
  @Input() withAll = false;
  categoryList: string[] = [];
  // tslint:disable-next-line: no-output-rename
  @Output('update') newVal: EventEmitter<string> = new EventEmitter<string>();

  constructor(private config: AppConfigService) {
  }

  ngOnInit(): void {
    if (this.withAll === undefined) {
      this.withAll = false;
    }

    if ( this.options === null ) {
      console.assert(false);
    }
    const orgList = this.options;
    if (this.withAll) {
      this.categoryList.push('all');
      orgList.forEach(category => {
        this.categoryList.push(category);
      });
    } else {
      this.categoryList = orgList;
    }
  }

  catChanged() {
    const newCat = this.inCatName;
    this.newVal.emit(newCat);
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CAbsCategory } from '../../../../gam_ve_gam/C_AbsCategory';
import { CAbsFeature } from '../../../../gam_ve_gam/C_AbsFeature';
import { CUser } from '../../../../gam_ve_gam/CUser';

@Component({
  selector: 'app-tag-item-category',
  templateUrl: './tag-item-category.component.html',
  styleUrls: ['./tag-item-category.component.css']
})
export class TagItemCategoryComponent implements OnInit {
  @Input() inCategory: CAbsCategory | undefined;
  @Input() inOptions: CAbsCategory | undefined;

  // tslint:disable-next-line: no-output-rename
  @Output('update') newVal: EventEmitter<CAbsCategory> = new EventEmitter<CAbsCategory>();

  mShowOK = false;
  mCols = 1;
  originalCategory!: CAbsCategory;
  navigationSubscription: any;
  bChanged = false;
  theUser!: CUser;

  constructor() {
  }

  ngOnInit(): void {
    if (this.inCategory !== undefined) {
      this.originalCategory = this.inCategory.copyConstructor();
      this.mShowOK = this.inCategory.readyForWork();
    } else {
      console.assert(false);
    }
    if (this.inOptions === undefined) {
      console.assert(false);
    }
    this.mCols = 2;
    this.bChanged = false;
  }

  onUpdateFeature(modifed: CAbsFeature) {
    console.assert(modifed !== null, 'looking for a real updated feature');
    if (this.inCategory !== undefined) {
      this.inCategory.setCategory(modifed.name, modifed.vals);
      this.mShowOK = this.inCategory.readyForWork();
    }
  }

  onClickOK() {
    console.log('OK');
    if (this.originalCategory === this.inCategory) {
      console.log('Not changed.');
    } else {
      console.log('Category modified');
      this.newVal.emit(this.inCategory);
    }
  }

  somethingChanged(index: number) {
    if (this.inCategory !== undefined) {
      this.mShowOK = this.inCategory.readyForWork();
      this.bChanged = true;
    }
  }
}

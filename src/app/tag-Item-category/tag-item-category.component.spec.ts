import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagItemCategoryComponent } from './tag-item-category.component';

describe('TagCategoryComponent', () => {
  let component: TagItemCategoryComponent;
  let fixture: ComponentFixture<TagItemCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagItemCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagItemCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

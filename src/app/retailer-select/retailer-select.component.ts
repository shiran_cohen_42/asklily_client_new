import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CUser, I_UTR, I_UTRoptions } from '../../../../gam_ve_gam/CUser';
import { AppConfigService } from '../app.configService';
import { AskService } from '../ask.service';

@Component({
  selector: 'app-retailer-select',
  templateUrl: './retailer-select.component.html',
  styleUrls: ['./retailer-select.component.scss']
})
export class RetailerSelectComponent implements OnInit {
  // @Input() appUser?: CUser;
  // tslint:disable-next-line: no-output-rename
  @Output('update') retailerUpdated: EventEmitter<I_UTR> = new EventEmitter<I_UTR>();

  mCurRetailer = '';
  mCurUserType = '';
  mOptions: I_UTRoptions;
  mUserTypes: string[] = [];
  tmpUser!: CUser;
  constructor(private askService: AskService, private config: AppConfigService) {
    this.mOptions = this.config.getAppOptions();
  }

  ngOnInit(): void {
    const utr = this.config.getTempUTR();
    this.mOptions.userTypes = this.config.getAppUserTypes();
    // if ( appUser !== undefined ) {
      // const utr = this.appUser.getUTR();
    this.mCurUserType = utr.userType;
    this.mCurRetailer = utr.retailer;
  // } else {
    //   console.assert(false);
    // }
  }

  // userChanged() {
  //   this.retailerUpdated.emit({ userType: this.mCurUserType, retailer: this.mCurRetailer } );
  // }

  async retailerChanged() {
    const utr = { userType: this.mCurUserType, retailer: this.mCurRetailer };
    this.config.set(utr)
    .then(() => {
      this.retailerUpdated.emit( utr );
      this.ngOnInit();
    });
  }
}

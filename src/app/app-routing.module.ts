import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CitemTableComponent } from './item-table/item-table.component';
import { ItemViewComponent } from './item-view/item-view.component';
import { AsklilyComponent } from './asklily/asklily.component';
import { HomeBaseComponent } from './home-base/home-base.component';
import { PublicAskComponent } from './public-ask/public-ask.component';

import { AuthGuard } from './auth/auth.guard';
import { ImageProcessingComponent } from './image-processing/image-processing.component';
import { roles } from './auth/constants';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'home-base',
    canActivate: [AuthGuard],
    data: {
      roles: [roles.user, roles.admin],
      title: 'Strat point'
    },
    component: HomeBaseComponent,
  },
  {
    path: 'askLily',
    component: AsklilyComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [roles.admin, roles.user],
      title: 'AskLily Alg dev'
    }
  },
  {
    path: 'app-public-ask',
    canActivate: [AuthGuard],
    data:
    {
      roles: [roles.admin, roles.user],
      title: 'Public Api test'
    },
    component: PublicAskComponent
  },
  {
    path: 'item-table',
    component: CitemTableComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [roles.admin, roles.user, roles.stylist],
      title: 'Items table'
    }
  },
  {
    path: 'item-view/:id',
    component: ItemViewComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [roles.admin, roles.user, roles.stylist],
      title: 'Item editing',
      runGuardsAndResolvers: 'paramsChange'
    }
  },
  {
    path: 'image-processing',
    component: ImageProcessingComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [roles.admin, roles.user],
      title: 'AskLily Alg dev'
    }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

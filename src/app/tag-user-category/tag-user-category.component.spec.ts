import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagUserCategoryComponent } from './tag-user-category.component';

describe('TagCategoryComponent', () => {
  let component: TagUserCategoryComponent;
  let fixture: ComponentFixture<TagUserCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagUserCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagUserCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

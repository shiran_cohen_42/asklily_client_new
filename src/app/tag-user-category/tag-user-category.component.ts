import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CAbsCategory } from '../../../../gam_ve_gam/C_AbsCategory';
import { Router, NavigationEnd } from '@angular/router';
import { CAbsFeature, eFeatureType } from '../../../../gam_ve_gam/C_AbsFeature';
import { CUser } from '../../../../gam_ve_gam/CUser';

@Component({
  selector: 'app-tag-user-category',
  templateUrl: './tag-user-category.component.html',
  styleUrls: ['./tag-user-category.component.css']
})
export class TagUserCategoryComponent implements OnInit {
  @Input() inUser: CUser | undefined;

  // tslint:disable-next-line: no-output-rename
  @Output('update') newVal: EventEmitter<CAbsCategory> = new EventEmitter<CAbsCategory>();

  mShowOK = false;
  mCols = 1;
  originalCategory!: CAbsCategory;
  navigationSubscription: any;
  bChanged = false;
  theUser!: CUser;
  mDisplayFeatureIndexes: number[] = [];
  inCategory!: CAbsCategory ;
  inOptions!: CAbsCategory;

  constructor() {
  }


  ngOnInit(): void {

    if ( this.inUser !== undefined ) {
      this.inCategory = this.inUser.getUserCopy();
      this.inOptions = this.inUser.getOptionsCopy();
      const toRemove: number[] = [];
      this.inCategory.vals.forEach( (f, i) => {
        if ( !f.isDisplayable() ) {
          toRemove.push(i);
        }
      });

      // now reomve last to first
      for ( let i = toRemove.length -1 ; i > -1 ; i--) {
        this.inCategory?.vals.splice(i, 1);
        this.inOptions?.vals.splice(i, 1);
      }
      this.originalCategory = this.inUser.getUser();
      this.mShowOK = this.inUser.isReady();
    }

    this.mCols = 2;
    this.bChanged = false;
  }

  onUpdateFeature(modifed: CAbsFeature) {
    console.assert(modifed !== null, 'looking for a real updated feature');
    if (this.inCategory !== undefined) {
      this.inCategory.setCategory(modifed.name, modifed.vals);

      if ( this.inUser !== undefined ) {
        this.inUser.updateFeature(modifed.name, modifed.vals);
        this.mShowOK = this.inUser.isReady();
      }
    }
  }

  onClickOK() {
    console.log('OK');
    if (this.originalCategory === this.inCategory) {
      console.log('Not changed.');
    } else {
      console.log('Category modified');
      this.newVal.emit(this.inUser?.getUser());
    }
  }

  somethingChanged(index: number) {
    if (this.inCategory !== undefined) {
      this.mShowOK = this.inCategory.readyForWork();
      this.bChanged = true;
    }
  }


  toDisplayFeature(j: number): boolean {
    if ( this.inCategory !== undefined ) {
      return this.inCategory.vals[j].isDisplayable();
    } else {
      return false;
    }
  }
}

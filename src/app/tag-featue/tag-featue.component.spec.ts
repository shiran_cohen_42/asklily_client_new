import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagFeatueComponent } from './tag-featue.component';

describe('TagFeatueComponent', () => {
  let component: TagFeatueComponent;
  let fixture: ComponentFixture<TagFeatueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagFeatueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagFeatueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CAbsFeature } from '../../../../gam_ve_gam/C_AbsFeature';
import { AppConfigService } from '../app.configService';

@Component({
  selector: 'app-tag-featue',
  templateUrl: './tag-featue.component.html',
  styleUrls: ['./tag-featue.component.scss']
})
export class TagFeatueComponent implements OnInit {
  @Input() inFeature!: CAbsFeature;
  @Input() inOptions!: CAbsFeature;

  @Input() belogsToItem!: boolean;
  // tslint:disable-next-line: no-output-rename
  @Output('update') newVal: EventEmitter<CAbsFeature> = new EventEmitter<CAbsFeature>();

  mFeature!: CAbsFeature;

  constructor(private config: AppConfigService) {
  }

  ngOnInit(): void {
    // copy the inFeature so it can be modyfied without saving
    this.mFeature = this.inFeature.copy();
    // console.log('Editing ', this.mFeature.name, ' is item: ', this.belogsToItem);
  }

  isFeatureMultipleSelect(): boolean {
    console.assert(this.mFeature !== null, 'In Feature not valid');
    const rc = this.inOptions.getNumOption(this.belogsToItem);
    if (rc > 1) {
      return true;
    } else {
      return false;
    }
  }

  getCurValues(): string[] {
    console.log(this.mFeature);
    return this.mFeature.getCurValues(this.belogsToItem);
  }

  getCurValue(): string {
    return this.mFeature.getCurValue();
  }

  getValueForTag(tag: string, value: boolean): string {
    return this.mFeature.getStrForOptions(tag, value);
  }

  onFeatureChange() {
    const tooLong = this.mFeature.isDataTooLong(this.belogsToItem);
    if (tooLong) { return; }

    this.inFeature.vals = this.mFeature.vals;
    if (this.inFeature.readyForWork()) {
      this.newVal.emit(this.inFeature);
    }
  }

}

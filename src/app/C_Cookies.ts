import { CookieService } from 'ngx-cookie-service';
import { I_UTR } from '../../../gam_ve_gam/CUser';
import { CAbsCategory } from '../../../gam_ve_gam/C_AbsCategory';
import { CAbsFeature, eFeatureType } from '../../../gam_ve_gam/C_AbsFeature';


// tslint:disable-next-line: class-name
export class C_Cookies {
    utrCookie = 'appUtr';
    mName = '';
    mCs: any;
    constructor(cookieService: CookieService) {
        this.mCs = cookieService;
     }

    check(name: string): boolean {
        return this.mCs.check(name);
    }

    getUTR(): I_UTR | undefined {
        if (this.check(this.utrCookie)) {
            const cookie = this.mCs.get(this.utrCookie);
            const utr = JSON.parse(cookie);
            return utr;
        }
        return undefined;
    }

    getUserFromCooclies(name: string): CAbsCategory | undefined {
        if (this.check(name)) {
            const cookie = this.mCs.get(name);
            const a = JSON.parse(cookie);
            const user = new CAbsCategory(a.name, a.type, a.vals);
            return user;
        }
        return undefined;
    }

    saveUser(userToSave: CAbsCategory, underName: string): void {
        this.mName = underName;
        const tmp = JSON.stringify(userToSave);
        this.mCs.set(underName, tmp, 10);  // 10 days
    }

    saveUTR(utr: I_UTR): void {
        const tmp = JSON.stringify(utr);
        this.mCs.set(this.utrCookie, tmp, 10);  // 10 days
    }

    deleteUser(name: string): void {
        if (!this.check(name)) { return; }
        this.mCs.delete(this.mName);
    }

}

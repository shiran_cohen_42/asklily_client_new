import { CdbItem } from '../../../gam_ve_gam/C_DbItem';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { AppConfigService } from './app.configService';
import { Observable, Subject } from 'rxjs';  // Observable is used do not remove
import { I_UTR } from '../../../gam_ve_gam/CUser';

export class CTableData {
  itemId = '';
  tagged = false;
  category = '';
  constructor() { }
}

export class COptionSelectField {
  options: string[];
  selected: string;
  constructor() {
    this.options = ['all'];
    this.selected = 'all';
  }
}

export class CItemSelection {
  showUntaggedOnly: boolean;
  categories: COptionSelectField;
  constructor(showUntaggedOnly: boolean) {
    this.showUntaggedOnly = showUntaggedOnly;
    this.categories = new COptionSelectField();
  }
}


export class CappItem {
  item: CdbItem;
  forTag: boolean;
  inStock: boolean;
  constructor(dbItem: CdbItem) {
    this.item = dbItem;
    this.forTag = false;
    this.inStock = true;
  }

}
/*
The name will be passed to getAllClothes...etc
*/
@Injectable()
export class ItemsService {
  mItemList: CappItem[];
  private prevItemIndex = -1;
  private curItemIndex = -1;
  private nextItemIndex = -1;
  private dbData: CdbItem[] = [];
  serverResultSubject: Subject<any>;
  loadInProgress = true;
  updateInProgress = true;
  itemSelectOptions: any[] = [];

  constructor(private api: ApiService, private config: AppConfigService) {
    this.serverResultSubject = new Subject<any>();
    this.mItemList = [];
    this.curItemIndex = -1;
    this.itemSelectOptions = ['all', true];
  }

  loadingDone(): boolean {
    return this.loadInProgress;
  }

  updateDone(): boolean {
    return this.updateInProgress;
  }

  timer(ms: number) {
    return new Promise(res => setTimeout(res, ms));
  }

  public getAll(category: string): CdbItem[] {
    const rv: CdbItem[] = [];

    this.mItemList.forEach(appItem => {
      if (appItem.item.selected(category)) {
        rv.push(appItem.item);
      }
    });
    return rv;
  }

  async setCurrentItems(ut: I_UTR) {
    console.log('Loading initiated for: ', ut.userType, '+', ut.retailer);
    this.loadInProgress = true;
    this.mItemList = [];  // empty
    await this.api.getTheItems(ut)
      .then((res: CdbItem[]) => {
        this.dbData = res;

        this.dbData.forEach(item => {
          // const jItem = JSON.stringify(item);
          // const dbItem = new CdbItem(jItem);
          const appItem = new CappItem(item);
          this.mItemList.push(appItem);
        });
        console.log('Num records in DB is:', this.mItemList.length);
        this.dbData = [];
        this.loadInProgress = false;
      }, err => {
        console.log(err);
      });

  }

  /// adding or updating is here so it updates the mItemList
  updateItem(item: CdbItem) {
    console.assert(item._id !== null, 'Udating must have _id');
    this.updateInProgress = true;
    this.api.updateItem(item)
      .subscribe((res: any) => {
        this.serverResultSubject.next(res);
        const i = this.getIndex(item.mId);
        console.assert(i > -1, 'How come its not in....');
        this.mItemList[i].item = item;
        // console.log('Updated: ' + item.mId);
        this.updateInProgress = false;
      },
        error => {
          console.assert(false, 'Failed to update item...');
          console.log(error);
        }
      );
  }


  getItemSelectOptions(): any[] {
    return this.itemSelectOptions;
  }

  getRowData(selectedCategory: string, showAll: boolean) {
    this.itemSelectOptions = [selectedCategory, showAll];
    console.log('Finally getting row data');

    const rv: CTableData[] = [];

    this.mItemList.forEach((appItem: CappItem) => {
      appItem.forTag = false;
      if (this.toShow(appItem.item, selectedCategory, showAll)) {
        appItem.forTag = true;
        const row = new CTableData();
        row.category = appItem.item.categoryName();
        row.itemId = appItem.item.mId;
        row.tagged = appItem.item.mCategory.readyForWork();
        rv.push(row);
      }
    });
    return rv;

  }


  toShow(item: CdbItem, selectedCategory: string, showAll: boolean) {
    if (!item.selected(selectedCategory)) {
      return false;
    }

    if (showAll) {
      return true;
    } else {
      return !item.mCategory.readyForWork();
    }
  }

  setTaggedToNoStock() {
    this.mItemList.forEach(it => {
      if (it.forTag) {
        it.item.mStock = '';
      }
    });
  }

  getByIndex(i: number) {
    return this.mItemList[i].item;
  }

  getItem(id: string) {
    let i: number;
    for (i = 0; i < this.mItemList.length; i++) {
      if (this.mItemList[i].item.mId === id) {
        this.curItemIndex = i;
        this.setNextPrev();
        return this.mItemList[i].item;
      }
    }
    console.assert(false, 'How come I asked for something that does notexist');
    return null;
  }

  getIndex(id: string): number {
    let i: number;
    for (i = 0; i < this.mItemList.length; i++) {
      if (this.mItemList[i].item.mId === id) {
        return i;
      }
    }
    console.log(false, 'Not foud ' + id);
    return -1;
  }

  getItemIndex() {
    return this.curItemIndex;
  }

  setNextPrev() {
    this._setNextItem();
    this._setPrevItem();
  }

  _setNextItem() {
    for (let i = this.curItemIndex + 1; i < this.mItemList.length; i++) {
      if (this.mItemList[i].forTag) {
        this.nextItemIndex = i;
        console.assert(this.nextItemIndex < this.mItemList.length, 'Make sure index is ok');
        return;
      }
    }
  }

  _setPrevItem() {
    for (let i = this.curItemIndex - 1; i > -1; i--) {
      if (this.mItemList[i].forTag) {
        this.prevItemIndex = i;
        console.assert(this.prevItemIndex > -1, 'Make sure index is ok');
        return;
      }
    }
  }

  getNextItem() {
    return this.mItemList[this.nextItemIndex].item.mId;
  }

  getPrevItem() {
    return this.mItemList[this.prevItemIndex].item.mId;
  }

  async deleteItem(itemId: string) {
    // for now its the current item
    console.assert(itemId === this.mItemList[this.curItemIndex].item.mId, 'Should be the same one');
    this.api.deletecItem(itemId)
      .subscribe(
        (res: any) => {
          // in the call after this, there is a call to delete it from the DB
          this.mItemList.splice(this.curItemIndex, 1);
          this.setNextPrev();
        }, (err: any) => {
          console.assert(false, 'Failed to remove item...');
          console.log(err);
        }
      );
  }

  sendListToSerever(items: CdbItem[]) {
    this.api.addCCollection(items)
      .subscribe(
        data => {
          this.serverResultSubject.next(data);
          console.log('Collection was added: ' + items.length);
          items.forEach((i: CdbItem) => {
            const add = new CappItem(i);
            this.mItemList.push(add);
          });
        },
        error => { console.log(error); }
      );
  }

  addListToBD(items: CdbItem[]) {
    if (items.length < 1) {
      return;
    }
    const numItemsAtOneTime = 24;

    let itemToAdd: CdbItem[] = [];
    items.forEach(ele => {
      itemToAdd.push(ele);
      if (itemToAdd.length > numItemsAtOneTime) {
        this.sendListToSerever(itemToAdd);
        itemToAdd = [];
      }
    });
    // left a few?
    if (itemToAdd.length > 0) {
      this.sendListToSerever(itemToAdd);
    }
  }

}

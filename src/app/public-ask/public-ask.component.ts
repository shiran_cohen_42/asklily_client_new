import { Component, OnInit, Input } from '@angular/core';
import { AskService } from '../ask.service';
import { AppConfigService } from '../app.configService';
import { Observable, Subject } from 'rxjs';  // Observable is used do not remove
import { CAskLilyResults, CItemResult } from '../../../../gam_ve_gam/C_ResultDef';
import { CUser, I_UTR } from '../../../../gam_ve_gam/CUser';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-public-ask',
  templateUrl: './public-ask.component.html',
  styleUrls: ['./public-ask.component.scss']
})

export class PublicAskComponent implements OnInit {
  mRandomUser!: CUser;
  matchResults!: CAskLilyResults;
  showList = false;
  showNewList = false;
  data = '';
  loopIndex = 0;
  loadingResults = false;
  numValid = 0;
  showUserDetails = false;
  bShowImages = false;
  haveUser = false;
  resultList: CItemResult[][] = [];

  constructor(
    private askService: AskService,
    cookieService: CookieService,
    private config: AppConfigService) {
    }

  ngOnInit(): void {
    this.matchResults = new CAskLilyResults(false);
    this.showList = false;
    this.setNewRandomUser();
  }

  setNewRandomUser() {
    const utr = this.config.getTempUTR();
    this.mRandomUser = this.config.getArandomUser(utr);
    this.haveUser = true;
  }

  toggleShowImage() {
    this.bShowImages = !this.bShowImages;
  }

  onToggleShowDetails() {
    this.showUserDetails = !this.showUserDetails;
  }

  getLen(): number {
    let len = 0;
    this.matchResults.mList.forEach(category => {
      len += category.matchList.length;
    });
    return len;
  }

  async GetLilyList(show: boolean) {
    this.showList = this.showNewList = false;
    this.setNewRandomUser();
    this.matchResults = await this.askService.askLily(this.mRandomUser, false);
    this.numValid = this.getLen();
    this.resultList = this.matchResults.getRandomDisplayList(20);
    this.showNewList = show;
  }

  async loopTest() {
    const numLoops = 100;
    for (let i = 0; i < numLoops; i++) {
      const ak = await this.GetLilyList(false)
        .then(() => {
          console.log('can continue ' + i);
          this.loopIndex = i;
        })
        .catch();
      this.showList = false;
    }
  }

  onRetailerChange(event: any) {
    if (event !== undefined) {
      this.ngOnInit();
    }
  }

  getListLen(): number {
    return this.resultList.length / 2;
  }
}



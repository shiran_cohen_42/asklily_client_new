import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicAskComponent } from './public-ask.component';

describe('PublicAskComponent', () => {
  let component: PublicAskComponent;
  let fixture: ComponentFixture<PublicAskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicAskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

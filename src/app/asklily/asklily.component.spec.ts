import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsklilyComponent } from './asklily.component';

describe('AsklilyComponent', () => {
  let component: AsklilyComponent;
  let fixture: ComponentFixture<AsklilyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsklilyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsklilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { AppConfigService } from '../app.configService';
import { AskService } from '../ask.service';
import { CAskLilyResults } from '../../../../gam_ve_gam/C_ResultDef';
import { Observable, Subject } from 'rxjs';  // Observable is used do not remove
import { CUser, I_UTR } from '../../../../gam_ve_gam/CUser';
import { CookieService } from 'ngx-cookie-service';
import { C_Cookies } from '../C_Cookies';
import { BIT } from './BIT';


@Component({
  selector: 'app-asklily',
  templateUrl: './asklily.component.html',
  styleUrls: ['./asklily.component.scss']
})



export class AsklilyComponent implements OnInit {
  aUser!: CUser;
  showUser = false;
  matchResults!: CAskLilyResults;
  showList = false;
  totalItems = 0;
  totalMatch = 0;
  totalRejected = 0;
  debugAlgInfo = true;
  mCS: C_Cookies;
  haveUser = false;

  constructor(
    private askService: AskService,
    private config: AppConfigService,
    cookieService: CookieService) {

      this.mCS = new C_Cookies(cookieService);
  }

  async ngOnInit() {
    this.showUser = false;
    this.showList = false;
    await this.setUser()
      .then(() => {
        console.assert(this.aUser !== undefined);
        this.matchResults = new CAskLilyResults(true);
        if (!this.aUser.isReady()) {
          this.showUser = true;
        } else {
          this.GetLilyList()
            .then(() => {
              console.log('Got results');
            })
            .catch((err) => {
              console.log('failed! why?');
            });
        }
      });
  }

  async GetLilyList(): Promise<void> {
    if (!this.aUser.isReady()) {
      console.log('User not ready');
      return;
    }
    this.showList = false;
    await this.askService.askLily(this.aUser, this.debugAlgInfo)
      .then((data) => {
        this.matchResults = data;
        this.totalItems = this.matchResults.count();
        this.showList = true;
        return;
      });
  }


  onDeleteCookies() {
    // this.config.deleteUser();
  }

  onUpdateCategory(event: any) {
    this.aUser.update(event);
    const ready = this.aUser.isReady();
    this.saveInCookie();
    this.ngOnInit();
  }

  onShowUser() {
    this.showUser = !this.showUser;
  }

  onRetailerChange(event: any) {
    if (event !== undefined) {
      this.ngOnInit();
    }
  }

  saveBIT() {
    const bit = new BIT();
    bit.saveBIT(this.matchResults, this.aUser.getUser());
  }

  saveInCookie() {
    this.mCS.saveUser(this.aUser.getUser(), this.aUser.getUT());
  }

  async newUser() {
    const defUtr = this.config.getTempUTR();
    await this.config.getAUser(defUtr)
      .then((data) => {
        if (data !== undefined) {
          this.aUser = data;
          this.haveUser = true;
        }
      });
  }

  async setUser(): Promise<void> {
    await this.newUser()
      .then(() => {
        const cn = this.aUser.getUT();
        const savedUserInCookies = this.mCS.getUserFromCooclies(cn);
        if (savedUserInCookies !== undefined) {
          this.aUser.update(savedUserInCookies);
        }
      });
  }
}

import { CAbsCategory } from '../../../../gam_ve_gam/C_AbsCategory';
import { CAskLilyResults } from '../../../../gam_ve_gam/C_ResultDef';

export class BIT {

    constructor() {}

    saveBIT( saveResult: CAskLilyResults, user: CAbsCategory ) {
        const newList: any[] = [];
        saveResult.mList.forEach(category => {
            category.matchList.forEach(res => {
                newList.push([res.id, res.bMatch]);
            });
        });

        newList.sort((a, b) => a[0] - b[0]);

        const str = JSON.stringify(newList) + JSON.stringify(user);
        this.writeContents(str, 'bit.json');
    }

    writeContents(content: any, fileName: string) {
        const a = document.createElement('a');
        const file = new Blob([content], { type: 'text/plain' });
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
    }


}

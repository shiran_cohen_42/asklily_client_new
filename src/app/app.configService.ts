import { Injectable } from '@angular/core';
import { CUser, I_UTR, I_UTRoptions } from '../../../gam_ve_gam/CUser';
import { AskService } from './ask.service';
import { C_UserTypeConfig, I_Category_Item } from '../../../gam_ve_gam/C_UserTypeConfig';
import { C_RetailerConfig } from '../../../gam_ve_gam/C_RetailerConfig';
import { CookieService } from 'ngx-cookie-service';
import { C_Cookies } from './C_Cookies';


@Injectable({
  providedIn: 'root'
})

@Injectable()
export class AppConfigService {

  private mUtConfig!: C_UserTypeConfig;
  private mRetailer!: C_RetailerConfig;
  private mRetalierList: string[] = [];
  private tempUT = '';
  private tempRetailer = '';

  constructor(private cookieService: CookieService, private askService: AskService) {
  }

  async init() {
    if ( this.ready()) {
      return;
    }
    this.serverGetRetailerList()
      .then(() => {
        const cs = new C_Cookies(this.cookieService);
        const utr = cs.getUTR();
        let ret = this.mRetalierList[0];
        if ( utr !== undefined) {
          if ( utr.retailer !== undefined ) {
            ret = utr.retailer;
          }
          this.tempUT = utr.userType;
          this.tempRetailer = utr.retailer;
        }
        this.serverGetRetailer(ret)  // for sanity check
          .then(() => {
            this.serverGetCatalog()
              .then(() => {
                this.validateUsers();
                console.log('AppConfig is ready for action');
              });
          });
      });
  }

  async serverGetRetailerList() {
    console.log('Send server for retailer list...');
    await this.askService.askRetailerList()
      .then(list => {
        this.mRetalierList = list;
        console.log('AppConfig - retailer list', list);
      },
        err => {
          console.log('Failed to get retailer lis', err);
        });
  }

  async serverGetRetailer(retailerName: string) {
    console.log('Send server change retailer...');
    await this.askService.askRetailerGuiConfig(retailerName)
      .then(data => {
        this.mRetailer = new C_RetailerConfig(data);
        console.assert(retailerName === this.mRetailer.getName());
        console.log('Config Reatiler set to:', retailerName);
        this.tempRetailer = retailerName;
      },
        err => {
          console.log('Failed to get retailer config', err);
        });
  }

  async serverGetCatalog() {
    console.log('Send server for catalogs...');
    await this.askService.askCatalogs()
      .then(catalogs => {
        this.mUtConfig = new C_UserTypeConfig(catalogs);
        console.log('Config Catalogs set');
      },
        err => {
          console.log('Failed to get catalogs', err);
        });
  }


  ready(): boolean {
    return (this.mUtConfig !== undefined &&
      this.mRetailer !== undefined &&
      this.mRetalierList.length > 0);
  }

  valid(): void {
    console.assert(this.ready());
  }

  getAppOptions(): I_UTRoptions {
    this.valid();
    const ut = this.mUtConfig.getUserTypes();
    const rt = this.mRetalierList;
    return { userTypes: ut, retailers: rt } as I_UTRoptions;
  }

  // make sure the userTypes by a retailer are in catalogs
  private validateUsers() {
    this.valid();
    const retailerUser = this.mRetailer.getUserTypes();
    const systemTypes = this.mUtConfig.getUserTypes();
    retailerUser.forEach(userType => {
      let found = false;
      systemTypes.forEach(ut => {
        if (userType === ut) {
          found = true;
        }
      });
      console.assert(found);
    });

  }

  getAppUserTypes(): string[] {
    return this.mRetailer.getUserTypes();
  }

  async getAUser(utr: I_UTR): Promise<CUser> {
    if ( utr.userType === '') {
      utr.userType = this.mUtConfig.getDeault();
    }

    let rv: any;
    const full = this.mUtConfig.getItemOptions('User', utr.userType);
    const work = full.generateWorkFromOptions();
    rv = new CUser(work, full, this.mRetailer.getRetailer());
    return rv;
  }

  async set(utr: I_UTR) {
    let needReload = true;
    if (this.mRetailer.ready()) {
      if (this.mRetailer.getRetailer().retailerName === utr.retailer) {
        needReload = false;
      }
    }

    if (needReload) {
      await this.serverGetRetailer(utr.retailer)
        .then(() => {
          console.log('Config==> new retailer: ', this.mRetailer.getName());

          // check if UT belogs to retailer
          const retailerUT = this.mRetailer.getUserTypes();
          if ( utr.userType in retailerUT ) {
            this.tempUT = utr.userType;
          } else {
            this.tempUT = retailerUT[0];
          }
        });
    } else {
      // for next... app - but first check if retailer has this ut
      this.tempUT = utr.userType;
      console.log('Config: same retailer: ', this.mRetailer.getName());
    }

    this.saveCookies();
  }

  saveCookies() {
    const cs = new C_Cookies(this.cookieService);
    cs.saveUTR(this.getTempUTR());
  }

  getCategoryList(ut: string): string[] {
    const rv = this.mUtConfig.getCategoryList(ut);
    return rv;
  }

  getRetailerName(): string {
    console.assert(this.mRetailer !== undefined);
    return this.mRetailer.getRetailer().retailerName;
  }

  private modifyRetailerName(newRetailer: string) {
    // in random testing I need the retailer name only
    this.mRetailer.getRetailer().retailerName = newRetailer;
  }

  getArandomUser(utr: I_UTR): CUser {
    const i = this.mUtConfig.ut2i(utr.userType);
    const ut = this.mUtConfig.getUt(i);
    const full = this.mUtConfig.getItemOptions('User', ut);
    const rand = full.generateRandom();
    // randon User - modify name
    this.modifyRetailerName(utr.retailer);
    const randUser = new CUser(rand, full, this.mRetailer.getRetailer());
    return randUser;
  }

  getCategoryOptions(categoryName: string, ut: string): I_Category_Item {
    const rv = this.mUtConfig.getUT_Item(ut, categoryName);
    return rv;
  }

  getUti(userType: string): number {
    return this.mUtConfig.ut2i(userType);
  }

  getDefaultUTR(): I_UTR {
    let ret = this.mRetalierList[0];
    if (this.mRetailer !== undefined) {
      ret = this.mRetailer.getName();
    }
    return { userType: this.mUtConfig.getUt(0), retailer: ret } as I_UTR;
  }

  // setTemp(utr: I_UTR) {
  //   // should be used in the application to change
  //   this.tempUT = utr.userType;
  //   this.tempRetailer = utr.retailer;
  // }

  getTempUTR(): I_UTR {
    if ( this.tempUT === '') {
      this.tempUT = this.mUtConfig.getUt(0);
    }

    if ( this.tempRetailer === '') {
      let ret = this.mRetalierList[0];
      if (this.mRetailer !== undefined) {
        ret = this.mRetailer.getName();
      }
      this.tempRetailer = ret;
    }
    return { userType: this.tempUT, retailer: this.tempRetailer} as I_UTR;
  }

}

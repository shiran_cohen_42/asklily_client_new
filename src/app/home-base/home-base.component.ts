import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../app.itemsService';
import { AppConfigService } from '../app.configService';

@Component({
  selector: 'app-home-base',
  templateUrl: './home-base.component.html',
  styleUrls: ['./home-base.component.scss']
})
export class HomeBaseComponent implements OnInit {
  dbLen = -1;
  isConfigReady = false;
  constructor(private items: ItemsService, private config: AppConfigService) {
    this.initConfig();
  }

  ngOnInit() { }

  async waitForConfig(): Promise<void> {
    do {
      await new Promise(res => setTimeout(res, 25));
      console.log('*');
    } while (!this.config.ready());
  }

  async initConfig() {
    this.config.init();
    await this.waitForConfig()
    .then( () => {
      this.isConfigReady = true;
      console.log('Home: iniit config done');
    });
  }

}

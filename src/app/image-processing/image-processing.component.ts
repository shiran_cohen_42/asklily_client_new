import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../app.itemsService';
import { AppConfigService } from '../app.configService';
import { CdbItem } from '../../../../gam_ve_gam/C_DbItem';
import { CUser, I_UTR } from '../../../../gam_ve_gam/CUser';

// tslint:disable-next-line: class-name
class itemExport {
  category = '';
  url = '';
  constructor(c: string, u: string) {
    this.category = c;
    this.url = u;
  }
}

@Component({
  selector: 'app-image-processing',
  templateUrl: './image-processing.component.html',
  styleUrls: ['./image-processing.component.scss']
})
export class ImageProcessingComponent implements OnInit {
  mSelectedCategory = 'all';
  mItems: CdbItem[] = [];
  aUser!: CUser;
  haveUser = false;
  mCategoryList: string[] = [];
  haveList = false;

  constructor(private itemService: ItemsService, private config: AppConfigService) {}


  async newUser() {
    const defUtr = this.config.getTempUTR();
    await this.config.getAUser(defUtr)
      .then((data) => {
        if (data !== undefined) {
          this.aUser = data;
          this.haveUser = true;
          const utr = this.aUser.getUTR();
          this.mCategoryList = this.config.getCategoryList(utr.userType);
          this.haveList = true;
        }
      });
  }

  async ngOnInit() {
    console.log('On init image processing');
    await this.newUser()
    .then(() => {
      const utr = this.config.getTempUTR();
      this.itemService.setCurrentItems(utr)
      .then( () => {
        this.mItems = this.itemService.getAll(this.mSelectedCategory);
        })
      .catch( error => console.log(error));
    });
  }


  async doneLoading(): Promise<void> {
    console.log('Waiting for load to complete');
    while ( this.itemService.loadInProgress ) {
      await new Promise(res => setTimeout(res, 100));
      console.log('.');
    }
    console.log('Load compleated');
  }

  onRetailerChange(event: any) {
    this.ngOnInit();
  }

  onSelectShowCategory(event: any) {
    this.mSelectedCategory = event;
    this.ngOnInit();
  }


  checkItem( it: CdbItem): boolean {
    console.assert( it !== undefined );
    console.assert( it.mCategory !== undefined );
    if ( this.mSelectedCategory === 'all' ) {
      return true;
    }

    if ( it.mCategory.name === this.mSelectedCategory ) {
      return true;
    }
    return false;
  }

  onOutput() {
    if (this.mItems.length === 0) {
      confirm('No items for category: ' + this.mSelectedCategory);
      return;
    }
    const out: any[] = [];
    this.mItems.forEach( item => {
      if ( this.checkItem(item)) {
        const len = item.mImages.length;
        const it = new itemExport(item.categoryName(), item.mImages[len - 1]);
        out.push( it );
      }
    });

    const str = JSON.stringify(out);
    this.writeContents(str, 'categoryPage.json');

  }

  writeContents(content: any, fileName: string) {
    const a = document.createElement('a');
    const file = new Blob([content], { type: 'text/plain' });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
  }

}

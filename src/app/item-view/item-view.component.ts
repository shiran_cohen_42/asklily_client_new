import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ApiService } from '../api.service';
import { CdbItem } from '../../../../gam_ve_gam/C_DbItem';
import { ItemsService } from '../app.itemsService';
import { AppConfigService } from '../app.configService';
import { CAbsCategory } from '../../../../gam_ve_gam/C_AbsCategory';
import { I_Category_Item } from '../../../../gam_ve_gam/C_UserTypeConfig';

@Component({
  selector: 'app-item-view',
  templateUrl: './item-view.component.html',
  styleUrls: ['./item-view.component.scss']
})
export class ItemViewComponent implements OnInit {
  ctx: any;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private itemService: ItemsService,
    private config: AppConfigService,
    private router: Router) {
    // The coded added below (in the constructor) was added to allow the routing from one item to the next
    // override the route reuse strategy
    // tslint:disable-next-line: only-arrow-functions
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        // trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
        // if you need to scroll back to top, here is the right place
        window.scrollTo(0, 0);
      }
    });
  }

  mCurItem!: CdbItem;
  iCI!: I_Category_Item;
  theRetailer = '';
  mImageIndex = 0;
  curId = '';
  bMultipleImages = false;
  bShowCategory = false;
  bItemModified = false;
  bItemSaved = false;
  mCategoryList: string[] = [];

  ngOnInit(): void {
    this.curId = this.route.snapshot.params.id;
    console.assert(this.curId.length > 0, 'id must be valid');
    this.setCurItem(this.curId);
    this.iCI = this.config.getCategoryOptions(this.mCurItem.categoryName(), this.mCurItem.mUT);
    this.iCI.work = this.mCurItem.mCategory;
    this.bMultipleImages = (this.mCurItem.mImages.length > 1);
    this.bShowCategory = false;
    this.bItemModified = false;
    this.bItemSaved = false;
    this.mCategoryList = this.config.getCategoryList(this.mCurItem.mUT);
  }

  setCurItem(id: string) {
    // Protection...
    if (id === null) {
      console.assert(false, 'still...');
      return;
    }
    const tmp = this.itemService.getItem(id);
    if (tmp !== null) {
      this.mCurItem = tmp;
    }
  }

  nextImage() {
    this.mImageIndex = (this.mImageIndex + 1) % this.mCurItem.mImages.length;
  }

  onCahngeCategory(modifed: boolean) {
    this.bItemModified = true;
  }

  onUpdateCategory(modifed: CAbsCategory) {
    console.assert(modifed !== null, 'looking for a real updated category');
    this.mCurItem.mCategory.updateWith(modifed);
    this.itemService.updateItem(this.mCurItem);
    this.bItemSaved = true;
  }

  onClickReplaceCategory() {
    this.bShowCategory = true;
  }

  onCategoryNameChange(newCategoryName: string) {
    if (this.mCurItem.mCategory.name === newCategoryName) {
      return;
    } // no change

    const newCat = this.config.getCategoryOptions(newCategoryName, this.mCurItem.mUT);
    this.mCurItem.mCategory.updateWith(newCat.work);
    this.itemService.updateItem(this.mCurItem);
    this.bShowCategory = false;
    this.ngOnInit();
  }

  onClickDeleteItem() {
    if (!confirm('Are you sure you want to delete the item?')) {
      return;
    }
    console.log('Item about to be deleted');
    this.itemService.deleteItem(this.mCurItem._id)
      .then(() => {
        const id = this.itemService.getNextItem();
        this.router.navigate(['/item-view/', id]);
      });
  }

  moveOn(where: string) {
    let moveOn = true;
    if (this.bItemModified) {
      if (!this.bItemSaved) {
        const str = 'Want to move to ' + where + ' without saving?';
        if (!confirm(str)) {
          moveOn = false;
        }
      }
    }
    return moveOn;
  }

  onClickNext() {
    if (!this.moveOn('next')) { return; }
    const id = this.itemService.getNextItem();
    if (id === this.mCurItem.mId) {
      confirm('At end....');
      return;
    }
    this.router.navigate(['/item-view/', id]);
  }

  onClickPrev() {
    if (!this.moveOn('previous')) { return; }
    const id = this.itemService.getPrevItem();
    if (id === this.mCurItem.mId) {
      confirm('At start....');
      return;
    }
    this.router.navigate(['/item-view/', id]);
  }

  onOpps() {
    console.log('Ooops');
    const id = this.itemService.getPrevItem();
    this.router.navigate(['/item-view/', id]);
  }
  drawRectangle() {
    this.ctx.fillRect(20, 20, 200, 200);
  }
}

export const CRetailerConfig = {
    userTypes : [ 'female', 'male'],
    retailerName: 'rinazin',
    currentUserType: 'female',

    // font
    $fontFamily: 'Alef',
    $fontUrl: 'https://fonts.googleapis.com/css2?family=Alef&display=swap',
    displayLang: 'heb',
    textDirection: 'rtl',

    lessThanShortLegsCM: 170,
    greaterThanLongLegs: 185,
    lessThanAverageBMI: 19,
    greaterThanAverageBMI: 25,
    currencyCode: '\u20AA',

    baseRetailerUrl: 'https://res.cloudinary.com/ask-lily/image/upload/v1601395859/WC/retailer/rinazin/',
    logo_url: 'logo/rinazin_logo.png',

    stylesImagesUrl: 'Styles/',

    imagesForFemale: [
        'Professional_1.png',
        'Professional_2.png',
        'Rocker_1.png',
        'Rocker_2.png',
        'Minimalist_1.png',
        'Minimalist_2.png',
        'Fashionista_1.png',
        'Fashionista_2.png',
        'Casual_1.png',
        'Casual_2.png',
        'Boho_1.png',
        'Boho_2.png'
    ],
    imagesForMale: [],
};

# MEAN Stack Angular 9 Build Realtime CRUD Web App Quickly

To install Node JS, run the following command from the command line or from PowerShell:
choco install nodejs --version=12.18.2
NODE VERSION 12.8.2 ONLY!!!!

This source code is part of [MEAN Stack Angular 9 Build Realtime CRUD Web App Quickly](https://www.djamware.com/post/5e50b735525fc968b04a707f/mean-stack-angular-9-build-realtime-crud-web-app-quickly).

You must start server before...

1. cd AskLily\asklily_client_new
1.0 npm install (once)
1.1 ng serve --port 8080
1.2 Debug or Run

Some where in the application you need to edit user
<div *ngIf="showUser">
  <app-tag-user [inCategory]="mUser" (update)="onUpdateCategory($event)"></app-tag-user>
</div>


which invokes loop on all features in user

<mat-grid-tile *ngFor="let feature of inCategory.vals;  index as j" [colspan]="2">
  <app-tag-featue 
    [inFeature]="feature" 
    [belogsToItem]="false"
    (update)="onUpdateFeature($event)">
  </app-tag-featue>
</mat-grid-tile>

which invokes tag-feature.component:

<mat-select 
  [(value)]="mFeature.vals[0]"    // single value example
  name="mFeature.name" 
  (selectionChange)="onFeatureChange()">
  <mat-option *ngFor="let tag of mOptionsData.vals" 
    [value]="getValueForTag(tag, true)">
    {{getValueForTag(tag, false)}}
  </mat-option>
</mat-select>

on feature change :
```javascript
onFeatureChange() {
    const tooLong = this.mFeature.isDataTooLong(this.belogsToItem);
    if (tooLong) { return; }

    this.inFeature.vals = this.mFeature.vals;
    if (this.inFeature.readyForWork()) {
      this.newVal.emit(this.inFeature);
    }
  }
```

since we declare the emitted value is cought by
```javascript 
(update)="onUpdateFeature($event)"
```

```javascript
@Output('update') newVal: EventEmitter<CAbsFeature> = new EventEmitter<CAbsFeature>();
```

the udate function in tag-user.component.ts 

```javascript
onUpdateFeature(modifed: CAbsFeature) {
    console.assert(modifed !== null, 'looking for a real updated feature');
    this.inCategory.set(modifed.name, modifed.vals);
    this.inCategory.setSizeIfPossible();
    this.mShowOK = this.inCategory.readyForWork();
  }
```
